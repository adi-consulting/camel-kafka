# GET STARTED WITH APACHE CAMEL KAFKA
## TOOLS AND TECHNOLOGIES

- **Docker**
> We will be using the Landloop\fast-data-dev image
> https://hub.docker.com/r/landoop/fast-data-dev
- **MySQL**
- **Java IDE**
-  **Maven**
- **Spring Boot**
> To initialize the project **Spring Initializr** we will use
> https://start.spring.io/
> - Project : Maven Project
> - Language : Java
> - Version : 2.6.2
> - Dependencies : Spring Web

## INITIALIZE LANDOOP'S KAFKA DOCKER IMAGE ON WINDOWS

With a `docker-compose.yml`
```
version: '2'

services:
# this is our kafka cluster.
kafka-cluster:
image: landoop/fast-data-dev:cp3.3.0
environment:
	ADV_HOST: 127.0.0.1  # Change to 192.168.99.100 if using 	Docker Toolbox
	RUNTESTS: 0  # Disable Running tests so the cluster starts faster
	FORWARDLOGS: 0  # Disable running 5 file source connectors that bring application logs into Kafka topics
	SAMPLEDATA: 0  # Do not create sea_vessel_position_reports, nyc_yellow_taxi_trip_data, reddit_posts topics with sample Avro records.
ports:
	- 2181:2181  # Zookeeper
	- 3030:3030  # Landoop UI
	- 8081-8083:8081-8083  # REST Proxy, Schema Registry, Kafka Connect ports
	- 9581-9585:9581-9585  # JMX Ports
	- 9092:9092  # Kafka Broker
```

And run this command line using **command prompt** in the folder path
```
docker-compose up
```
Or with comand line using **command prompt**
```
docker run --rm -it -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -e ADV_HOST=127.0.0.1 landoop/fast-data-dev
```


## DOCKER /KAFKA /SQL /MySQL COMMANDS
### Docker
Launch Kafka **command line**
```
docker run --rm -it --net=host landoop/fast-data-dev bash
```
### Kafka
Create a **Topic** by providing the Zookeeper port, number of partions and replication-factor
```
kafka-topics --zookeeper 127.0.0.1:2181 --create --topic mytopic --partitions 3 --replication-factor 1
```
List all kafka **Topics** by providing the Zookeeper port
```
kafka-topics --zookeeper 127.0.0.1:2181 --list
```

Start a **Producer** by providing the Kafka port, the topic name and from where to start consuming
```
kafka-console-producer --broker-list 127.0.0.1:9092 --topic mytopic [--from-beginning]
```
Start a **Consumer** by providing the Kafka port and the topic name 
```
kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic mytopic
```
### MySQL

**Source** https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-commands.html

Switch execution mode to **SQL**
```
\sql
```
Connect to a **MySQL server**
```
\connect root@localhost:3306
```

Specify the **Database** to use
```
\use [schemaName]
```

### SQL

**Source** https://www.codecademy.com/article/sql-commands

**Create** a  Database
```
CREATE DATABASE [dbName];
```

**Create** a Table
```
CREATE TABLE [tableName] ([column1] datatype);
```
**Select From** table
```
SELECT * FROM [tableName];
```

**Insert Into** table
```
INSERT INTO [tableName] ([column1]) VALUES ([value1]);
```



## KAFKA DEVELOPPEMENT ENVIRONNEMENT

This console can be start on the port 3030 `localhost:3030`

## SPRING MICROSERVICES
### Simple Producer
**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Spring Maven Project** with the configuration above in your IDE

Then add the below **dependency** in your `pom.xml`
```
<dependency>
	<groupId>org.apache.camel.springboot</groupId>
	<artifactId>camel-kafka-starter</artifactId>
	<version>x.x.x</version>
</dependency>
```

Configure the Kafka broker URL in the `application.properties`
```
camel.component.kafka.brokers=localhost:9092
```
**To produce  a simple message**

Create the following class `KafkaSenderRoute.java`
```
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class KafkaSenderRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("timer://foo?fixedRate=true&period=5000")
        .setBody(constant("Test Message"))
        .to("kafka:mytopic");
	}
}
    
```
This code will produce the message `The Message` every 5 seconds to the Kafka Cluster

**To produce  from a file**

Create the following class `KafkaSenderRoute.java`
```
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class KafkaSenderRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("file:files\\input")
        .to("kafka:mytopic");
	}
}
    
```
Create the following folders in your project
```
└───files
    └───input
```
Then create a `sample.txt` file
```
Test Message
```
Or create a `sample.json` file
```
{
	"company" : "ADI-Consulting",
	"name" : "Gautier"
}
```
Select `Run As > Java Application` on your main **Spring** class

Open http://localhost:3030/kafka-topics-ui/#/ and select your topic
You sould see the content of the message

### Simple Consumer
**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Spring Maven Project** with the configuration above in your IDE

Then add the below **dependency** in your `pom.xml`
```
<dependency>
	<groupId>org.apache.camel.springboot</groupId>
	<artifactId>camel-kafka-starter</artifactId>
	<version>x.x.x</version>
</dependency>
```

Configure the Kafka broker URL in the `application.properties`
```
camel.component.kafka.brokers=localhost:9092
```
Create the following class `KafkaReceiverRoute.java`
```

import org.apache.camel.builder.RouteBuilder;
import static org.apache.camel.LoggingLevel.ERROR;
import org.springframework.stereotype.Component;
@Component
public class KafkaReceiverRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
    	from("kafka:mytopic")
    	.log(ERROR, "[${header.kafka.OFFSET}][${body}]");
    }
}
```
\
Select `Run As > Java Application` on your main **Spring** class

Start a **Kafka Producer** and type :
```
Test Message
```

You should see in your **IDE console**
 ```
[0][Test Message]
```

### Update data to database

**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Spring Maven Project** with the configuration above in your IDE

Then add the below **dependency** in your `pom.xml`
```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
	<groupId>org.apache.camel</groupId>
	<artifactId>camel-jdbc</artifactId>
	<version>3.8.0</version>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.camel.springboot</groupId>
    <artifactId>camel-kafka-starter</artifactId>
    <version>3.8.0</version>
</dependency>
```

Configure the **MySQL** database properties in the `application.properties`
This properties works only for **MySQL service**
```
spring.datasource.url=jdbc:mysql://localhost/[dbName]?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false
spring.datasource.username=[user]
spring.datasource.password=[pwd]
spring.datasource.platform=mysql
spring.datasource.initialization-mode=always
```
\
You can find some configuration for other services here https://howtodoinjava.com/spring-boot2/datasource-configuration/

Create the following script `schema-mysql.sql` in `src/main/resources`
```
DROP TABLE IF EXISTS camelMessage;

CREATE TABLE camelMessage(
	message VARCHA R(1000) NOT NULL
);
```
This will delete the existing camelMessage table and create a new one on the application start

Create the following class `CamelUpdateRoute.java`
```
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class CamelUpdateRoute extends RouteBuilder {
    
	@Override
    public void configure() throws Exception {
		from("timer://foo?repeatCount=1")
		.setBody(constant("INSERT INTO camelMessage(message)values('Test Message')"))
		.to("jdbc:dataSource");
    	
    }
}
```
\
Select `Run As > Java Application` on your main **Spring** class
Go to the **MySQL Shell** and run this commands
```
\sql
\use [dbName]
SELECT * FROM camelMessage;
```

You should get the following result
```
+--------------+
| message      |
+--------------+
| Test Message |
+--------------+
```

### Fetch data from database

**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Spring Maven Project** with the configuration above in your IDE

Then add the below **dependency** in your `pom.xml`
```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
	<groupId>org.apache.camel</groupId>
	<artifactId>camel-jdbc</artifactId>
	<version>3.8.0</version>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.camel.springboot</groupId>
    <artifactId>camel-kafka-starter</artifactId>
    <version>3.8.0</version>
</dependency>
```

Configure the **MySQL** database properties in the `application.properties`
This properties works only for **MySQL service**
```
spring.datasource.url=jdbc:mysql://localhost/[dbName]?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false
spring.datasource.username=[user]
spring.datasource.password=[pwd]
spring.datasource.platform=mysql
spring.datasource.initialization-mode=always
```
\
You can find some configuration for other services here https://howtodoinjava.com/spring-boot2/datasource-configuration/

Create the following class `CamelFetchRoute.java`
```
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class CamelFetchRoute extends RouteBuilder {
    
	@Override
    public void configure() throws Exception {
		from("timer://foo?repeatCount=1")
		.setBody(constant("select * from camelMessage"))
	    .to("jdbc:dataSource")       
	    .log("Message: ${body}");
    	
    }
}
```
Run your Camel Database Update microservice
Then select `Run As > Java Application` on your main **Spring** class

You should see in your **IDE console**
 ```
Messages: [{message=Test Message}]
```

### Camel Kafka Consumer update data to database
**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Spring Maven Project** with the configuration above in your IDE

Then add the below **dependency** in your `pom.xml`
```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
	<groupId>org.apache.camel</groupId>
	<artifactId>camel-jdbc</artifactId>
	<version>3.8.0</version>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.camel.springboot</groupId>
    <artifactId>camel-kafka-starter</artifactId>
    <version>3.8.0</version>
</dependency>
```
Be careful to use the same version for all camel dependencies

Configure the **MySQL** database properties in the `application.properties`
This properties works only for **MySQL service**
```
spring.datasource.url=jdbc:mysql://localhost/[dbName]?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false
spring.datasource.username=[user]
spring.datasource.password=[pwd]
spring.datasource.platform=mysql
spring.datasource.initialization-mode=always
```
Create the following script `schema-mysql.sql` in `src/main/resources`
```
DROP TABLE IF EXISTS camelMessage;

CREATE TABLE camelMessage(
	message VARCHA R(1000) NOT NULL
);
```

Create the following class `ConsumeUpdateRoute.java`
```
import static org.apache.camel.LoggingLevel.ERROR;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ConsumeUpdateRoute extends RouteBuilder {
    
	@Override
    public void configure() throws Exception {
		from("kafka:mytopic")
    	.log(ERROR, "$[${header.kafka.OFFSET}][${body}]")
    	.process(new JDBCinsertProcessor())
    	.to("jdbc:dataSource");
      }
}
```
Create the class `JDBCinsertProcessor.java`
```
import java.util.Map;
import org.apache.camel.Exchange;

public class JDBCinsertProcessor implements org.apache.camel.Processor {

    public void process(Exchange exchange) throws Exception {
    	
    	String sqlTable = "kafkaMessage";

        //extract the body of the Kafka message
        String body  = exchange.getIn().getBody(String.class);
        
        //extract all headers
        Map<String, Object> headers = exchange.getIn().getHeaders();
        Object offset = headers.get("kafka.OFFSET");
        Object partition = headers.get("kafka.PARTITION");
        Object topic = headers.get("kafka.TOPIC");

        //create the SQL request
        String insertSQL = "INSERT INTO "+sqlTable+" values ('"+offset+"','" + body + "','" + topic + "')";

        exchange.getIn().setBody(insertSQL);
    }
}
```
Start a **Kafka Producer** and type :
```
Test Message
```
\
Select `Run As > Java Application` on your main **Spring** class

Start a **Kafka Producer** and type :
```
Test Message
```
Go to the **MySQL Shell** and run this commands
```
\sql
\use [dbName]
SELECT * FROM camelMessage;
```

You should get the following result
```
+--------+--------------+---------+
| offset | body         | topic   |
+--------+--------------+---------+
| 0      | Test Message | mytopic |
+--------+--------------+---------+
```

And you should see in your **IDE console**
 ```
[0][Test Message]
```

### Poc Camel Kafka
**Source** https://gitlab.com/adi-consulting/camel-kafka.git

First import the **Poc source code** you'll find with the above link in your IDE

The configuration of **MySQL** database properties is in the `application.properties`
This properties works only for **MySQL service**
Here the database's name is **EventsManager**
```
spring.datasource.url=jdbc:mysql://localhost/EventsManager?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.platform=mysql
spring.datasource.initialization-mode=always
```
This script will automatically initialize the tables you needs for this POC `schema-mysql.sql` in `src/main/resources`
```
DROP TABLE IF EXISTS orders;

CREATE TABLE orders(
    order_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    timestamp VARCHAR(20) NOT NULL,
    price INTEGER(15) NOT NULL,
    items INTEGER(5) NOT NULL,
    status VARCHAR(50) NOT NULL,
    type VARCHAR(50) NOT NULL,	
    PRIMARY KEY (order_id)
);
```
\
**CAMEL Services**
Camel routes in `EventsManagerService.java`
 
**Synchrone create order** route
```
 from("direct:synchroneEvent")
		.to("jdbc:dataSource")
		.setBody(constant("SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1"))
		.to("jdbc:dataSource")
		.setBody(simple("Order ${body} Created : " + System.currentTimeMillis()))
        .to("kafka:ordersStatus");
```
This route first post the order to Mysql DB with `.to("jdbc:dataSource")` then create a message to publish to kafka `.setBody(simple("Order ${body} Created : " + System.currentTimeMillis())) .to("kafka:ordersStatus");`
\
**Asynchrone create order** route
```
from("kafka:"+topic)
		.doTry().unmarshal(jsonDataFormat)
    	.setBody(simple("INSERT INTO orders(timestamp,price,items,status,type) "
    			+ "VALUES('${header.kafka.TIMESTAMP}',${body.price},${body.items},'pending','asynchronous')"))
    	.to("jdbc:dataSource")
    	.setBody(constant("SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1"))
		.to("jdbc:dataSource")
    	.setBody(simple("Order ${body} Created from kafka : " + System.currentTimeMillis()))
        .to("kafka:ordersStatus");
```
This will consumme kafka messages on the topic describe in the `application.properties` here `kafka.topic=orders`. As the message is in JSON format the route unmarshall it to be able to use data `.doTry().unmarshal(jsonDataFormat)` . Then post it to Mysql DB with `.setBody(simple("xxx")).to("jdbc:dataSource")`. To finish the process as the first route a custom message is created and published in Kafka `.setBody(simple("xxx")).to("kafka:ordersStatus");`
\

**POC in action**

Select `Run As > Java Application` on the `DemoCamelKafkaLandoopApplication` class

Then open your **Postman** workspace and import the **POC Camel** collection. You'll see three requests

First send the `Create order synchronous` request

On `http://localhost:3030/kafka-topics-ui/#/cluster/fast-data-dev/topic/n/ordersStatus/`a new order should appear

Go to the **MySQL Shell** and run this commands
```
\sql
\use EventsManager
SELECT * FROM orders;
```

You should get the following result
```
+----------+---------------+-------+-------+---------+--------------+
| order_id | timestamp     | price | items | status  | type         |
+----------+---------------+-------+-------+---------+--------------+
|        1 | 1645630878922 |   120 |     1 | pending |  synchronous |
+----------+---------------+-------+-------+---------+--------------+
```
\
Now let's test the `Create order asynchronous` request

On `http://localhost:3030/kafka-topics-ui/#/cluster/fast-data-dev/topic/n/orders/`a new order should appear with the data you sent

At this moment the **CAMEL  Asynchrone create order**  route will consumme the kafka message to create the order

On `http://localhost:3030/kafka-topics-ui/#/cluster/fast-data-dev/topic/n/ordersStatus/`a new order should appear

Go to the **MySQL Shell** and run this commands
```
\sql
\use EventsManager
SELECT * FROM orders;
```

You should get the following result
```
+----------+---------------+-------+-------+---------+--------------+
| order_id | timestamp     | price | items | status  | type         |
+----------+---------------+-------+-------+---------+--------------+
|        1 | 1645630878922 |   160 |    15 | pending |  synchronous |
|        2 | 1645630881801 |   120 |     1 | pending | asynchronous |
+----------+---------------+-------+-------+---------+--------------+
```
